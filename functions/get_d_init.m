function d_up_tstar_mu_alpha_u_k_init = get_d_init(k_bar, param, sne_param)
% Gets initial type-state distribution

% Initial urgency distribution per urgency type
prob_down_mu_up_u_init = param.prob_down_mu_up_u;

% Initial karma distribution - independent of agent types
switch sne_param.karma_initialization
    
    case 0
        % All agents have average karma k_bar
        sigma_up_k_init = zeros(sne_param.n_k, 1);
        sigma_up_k_init(sne_param.i_k_bar) = 1;

    case 1
        % Uniform distribution over [0 : 2 * k_bar]
        sigma_up_k_init = get_sigma_up_k_uniform(k_bar, sne_param);

end

% Initial type-state distribution
if sne_param.karma_initialization == 10     % Get from file
    load(sne_param.initialization_file, 'sne_d_up_tstar_mu_alpha_u_k');
    d_up_tstar_mu_alpha_u_k_init = sne_d_up_tstar_mu_alpha_u_k;
else
    d_up_tstar_mu_alpha_u_k_init = einsum('ijk,jl,mn->ijklm', param.g_up_tstar_mu_alpha, prob_down_mu_up_u_init, sigma_up_k_init);
end



end
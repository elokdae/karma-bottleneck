function zeta_down_tstar_mu_u_t_b = get_immediate_reward(t_q_down_t_o, psi_down_t_b_up_o, param, sne_param)
%Gets immediate reward 

% Schedule early/late delays
t_diff_down_tstar_t_o = zeros(param.n_tstar, param.T, sne_param.n_o);
for i_tstar = 1 : param.n_tstar
    t_diff_down_tstar_t_o(i_tstar,:,:) = param.tstar(i_tstar) - param.t - t_q_down_t_o;
end
t_early_down_tstar_t_o = t_diff_down_tstar_t_o;
t_early_down_tstar_t_o(t_early_down_tstar_t_o<0) = 0;
t_late_down_tstar_t_o = -t_diff_down_tstar_t_o;
t_late_down_tstar_t_o(t_late_down_tstar_t_o<0) = 0;

% Total delay cost
t_cost_down_tstar_t_o = param.alpha * einsum('il,jk->ijk', ones(param.n_tstar, 1), t_q_down_t_o)...
    + param.beta * t_early_down_tstar_t_o...
    + param.gamma * t_late_down_tstar_t_o;

zeta_down_tstar_mu_u_t_b = -einsum('iln,jk,lmn->ijklm', t_cost_down_tstar_t_o, param.U_down_mu, psi_down_t_b_up_o);

end

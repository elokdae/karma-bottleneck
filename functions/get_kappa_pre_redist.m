function kappa_down_k_b_o_up_kn_pre_redist = get_kappa_pre_redist(param, sne_param)
    % Karma transition function before redistribution implementation
    kappa_down_k_b_o_up_kn_pre_redist = zeros(sne_param.n_k, sne_param.n_k, sne_param.n_o, sne_param.n_k);
    
    switch param.transition_mechanism
        
        case 1
            % All pay bid to society
            for i_k = 1 : sne_param.n_k
                for i_b = 1 : i_k
                    kappa_down_k_b_o_up_kn_pre_redist(i_k,i_b,:,i_k-i_b+1) = 1;
                end
            end
        
        otherwise
            % Winners pay bid to society
            for i_k = 1 : sne_param.n_k
                for i_b = 1 : i_k
                    kappa_down_k_b_o_up_kn_pre_redist(i_k,i_b,1,i_k-i_b+1) = 1;
                    kappa_down_k_b_o_up_kn_pre_redist(i_k,i_b,2,i_k) = 1;
                end
            end

        
            
    end
end
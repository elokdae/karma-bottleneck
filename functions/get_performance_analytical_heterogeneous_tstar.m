function [t_q_bar_nom, t_q_bar_nom_1, t_q_bar_nom_2, c_bar_nom, c_bar_nom_1, c_bar_nom_2, t_q_bar_toll, t_q_bar_toll_1, t_q_bar_toll_2, c_bar_toll, c_bar_toll_1, c_bar_toll_2] = get_performance_analytical_heterogeneous_tstar(param)
% This function closely follows Arnott et al., A structural model of
% peak-period congestion: A traffic bottleneck with elastic demand

% Everything in this function is in units of minutes
s_min = param.s / param.dt;
s_1_min = param.s_1 / param.dt;
s_2_min = param.s_2 / param.dt;
alpha_min = param.alpha / param.dt;
beta_min = param.beta / param.dt;
gamma_min = param.gamma / param.dt;
delta_tstar = param.tstar_min(1) - param.tstar_min(2);


%% Nominal performance
c_star = beta_min * gamma_min / ((beta_min + gamma_min) * s_min);
u_bar = dot(param.g_up_mu, param.u_bar_down_mu);

if param.multi_peak
    t_q_bar_nom_1 = c_star / (2 * alpha_min)...
        + gamma_min / (2 * alpha_min) * (param.g_up_tstar(1) / s_min - delta_tstar)...
        - s_min / (8 * alpha_min * param.g_up_tstar(1)) * (beta_min * (c_star / gamma_min - param.g_up_tstar(1) / s_min - delta_tstar).^2 + gamma_min * (c_star / gamma_min + param.g_up_tstar(1) / s_min - delta_tstar).^2);
    t_q_bar_nom_2 = c_star / (2 * alpha_min)...
        + beta_min / (2 * alpha_min) * (param.g_up_tstar(2) / s_min - delta_tstar)...
        - s_min / (8 * alpha_min * param.g_up_tstar(2)) * (beta_min * (c_star / beta_min + param.g_up_tstar(2) / s_min - delta_tstar).^2 + gamma_min * (c_star / beta_min - param.g_up_tstar(2) / s_min - delta_tstar).^2);
    t_q_bar_nom = param.g_up_tstar(1) * t_q_bar_nom_1 + param.g_up_tstar(2) * t_q_bar_nom_2;
    
    c_bar_nom_1 = u_bar / 2 * (c_star + gamma_min * (param.g_up_tstar(1) / s_min - delta_tstar));
    c_bar_nom_2 = u_bar / 2 * (c_star + beta_min * (param.g_up_tstar(2) / s_min - delta_tstar));
    c_bar_nom = param.g_up_tstar(1) * c_bar_nom_1 + param.g_up_tstar(2) * c_bar_nom_2;
    
else
    t_q_bar_nom = c_star / (2 * alpha_min);
    t_q_bar_nom_1 = t_q_bar_nom + param.g_up_tstar(2) / param.g_up_tstar(1) * beta_min / (2 * alpha_min) * delta_tstar;
    t_q_bar_nom_2 = t_q_bar_nom - beta_min / (2 * alpha_min) * delta_tstar;
    
    c_bar_nom = u_bar * (c_star - param.g_up_tstar(2) * beta_min * delta_tstar);
    c_bar_nom_1 = u_bar * c_star;
    c_bar_nom_2 = u_bar * (c_star - beta_min * delta_tstar);
end

%% Tolling performance
prob_up_u = einsum('ij,ik->j', param.prob_down_mu_up_u, param.g_up_mu);
m = 0;
for i_u = param.n_u : -1 : 1
    if sum(prob_up_u(i_u:end)) >= s_1_min / s_min
        m = i_u;
        r = 1 / prob_up_u(m) * (s_2_min / s_min - sum(prob_up_u(1:i_u-1)));
        break;
    end
end
t_q_bar_toll_1_down_u = zeros(param.n_u, 1);
t_q_bar_toll_2_down_u = zeros(param.n_u, 1);
c_bar_toll_1_down_u = zeros(param.n_u, 1);
c_bar_toll_2_down_u = zeros(param.n_u, 1);

t_q_bar_toll = s_2_min / s_min * t_q_bar_nom;

for i_u = 1 : param.n_u
    if i_u < m
        t_q_bar_toll_1_down_u(i_u) = t_q_bar_nom_1;
        t_q_bar_toll_2_down_u(i_u) = t_q_bar_nom_2;

        c_bar_toll_1_down_u(i_u) = param.U_down_mu(1,i_u) * c_bar_nom_1 / u_bar;
        c_bar_toll_2_down_u(i_u) = param.U_down_mu(1,i_u) * c_bar_nom_2 / u_bar;
    elseif i_u == m
        t_q_bar_toll_1_down_u(i_u) = r * t_q_bar_nom_1;
        t_q_bar_toll_2_down_u(i_u) = r * t_q_bar_nom_2;

        c_bar_toll_1_down_u(i_u) = param.U_down_mu(1,i_u) * c_bar_nom_1 / u_bar * (1 - (1 - r) * s_min / (2 * s_1_min) * prob_up_u(i_u));
        c_bar_toll_2_down_u(i_u) = param.U_down_mu(1,i_u) * c_bar_nom_2 / u_bar * (1 - (1 - r) * s_min / (2 * s_1_min) * prob_up_u(i_u));
    else
        c_bar_toll_1_down_u(i_u) = param.U_down_mu(1,i_u) * c_bar_nom_1 / u_bar * (1 - s_min / (2 * s_1_min) * prob_up_u(i_u));
        c_bar_toll_2_down_u(i_u) = param.U_down_mu(1,i_u) * c_bar_nom_2 / u_bar * (1 - s_min / (2 * s_1_min) * prob_up_u(i_u));
    end
end

t_q_bar_toll_1 = dot(prob_up_u, t_q_bar_toll_1_down_u);
t_q_bar_toll_2 = dot(prob_up_u, t_q_bar_toll_2_down_u);

c_bar_toll_1 = dot(prob_up_u, c_bar_toll_1_down_u);
c_bar_toll_2 = dot(prob_up_u, c_bar_toll_2_down_u);

c_bar_toll = param.g_up_tstar(1) * c_bar_toll_1 + param.g_up_tstar(2) * c_bar_toll_2;

end

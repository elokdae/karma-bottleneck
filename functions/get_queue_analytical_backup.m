function [t_analytical, r_analytical_down_t_o, q_analytical_down_t_o, t_q_analytical_down_t_o, U, prob_up_tstar_u, t_early_fast_down_tstar_u, t_late_fast_down_tstar_u, r_analytical_down_tstar_u_t_o, p_analytical_down_t_o, t_q_bar_analytical_down_tstar_u, c_bar_analytical_down_tstar_u] = get_queue_analytical(param, sne_param)
% This function closely follows Arnott et al., A structural model of
% peak-period congestion: A traffic bottleneck with elastic demand

% Everything in this function is in units of minutes
s_min = param.s / param.dt;
s_1_min = param.s_1 / param.dt;
s_2_min = param.s_2 / param.dt;
alpha_min = param.alpha / param.dt;
beta_min = param.beta / param.dt;
gamma_min = param.gamma / param.dt;

%% Normalized equilibrium cost
c_star = beta_min * gamma_min / ((beta_min + gamma_min) * s_min);
c_star_down_tstar = zeros(param.n_tstar, 1);
if param.multi_peak
    c_star_down_tstar(1) = (c_star - gamma_min * (param.tstar_min(1) - param.tstar_min(2) - param.g_up_tstar(1) / s_min)) / 2;
    c_star_down_tstar(2) = (c_star - beta_min * (param.tstar_min(1) - param.tstar_min(2) - param.g_up_tstar(2) / s_min)) / 2;
else
    c_star_down_tstar(1) = c_star;
    if param.n_tstar > 1
        c_star_down_tstar(2) = c_star - beta_min * (param.tstar_min(1) - param.tstar_min(2));
    end
end

%% Get time range of bottleneck
if param.multi_peak
    t_start = mean(param.tstar_min) - (param.g_up_tstar(2) + gamma_min / (beta_min + gamma_min)) / (2 * s_min);
    t_end = mean(param.tstar_min) + (param.g_up_tstar(1) + beta_min / (beta_min + gamma_min)) / (2 * s_min);
else
    t_start = param.tstar_min(1) - c_star / beta_min;
    t_end = param.tstar_min(1) + c_star / gamma_min;
end
dt_analytical = 0.5;
t_analytical = (t_start : dt_analytical : t_end).';
n_t_analytical = length(t_analytical);


%% Fast lane departures conditioned on t* and urgency u
U = reshape(param.U_down_mu, [], 1);
U = unique(U);
U = sort(U);
n_u = length(U);

prob_up_tstar_u = zeros(param.n_tstar, n_u);
g_up_tstar_mu = sum(param.g_up_tstar_mu_alpha, 3);
for i_tstar = 1 : param.n_tstar
    for i_mu = 1 : param.n_mu
        for i_u = 1 : param.n_u
            i_u2 = find(U == param.U_down_mu(i_mu, i_u));
            prob_up_tstar_u(i_tstar,i_u2) = prob_up_tstar_u(i_tstar,i_u2) + g_up_tstar_mu(i_tstar,i_mu) * param.prob_down_mu_up_u(i_mu,i_u);
        end
    end
end

s_1_tot = s_1_min / s_min;

% Start/end departure times on the fast lane
fast_lane_intervals = cell(0);
t_early_fast_down_tstar_u = zeros(param.n_tstar, n_u + 1);
t_late_fast_down_tstar_u = zeros(param.n_tstar, n_u + 1);
t_early_fast_down_tstar_u(:,n_u + 1) = param.tstar_min;
t_late_fast_down_tstar_u(:,n_u + 1) = param.tstar_min;

m_down_tstar = zeros(param.n_tstar, 1);
remaining_m_down_tstar = zeros(param.n_tstar, 1);

if param.n_tstar == 1
    for i_u = n_u : -1 : 1
        t_early_fast_down_tstar_u(1,i_u) = param.tstar_min(1) - gamma_min / (beta_min + gamma_min) * (t_late_fast_down_tstar_u(1,i_u+1) - t_early_fast_down_tstar_u(1,i_u+1) + prob_up_tstar_u(1,i_u) / s_1_min);
        t_late_fast_down_tstar_u(1,i_u) = param.tstar_min(1) + beta_min / (beta_min + gamma_min) * (t_late_fast_down_tstar_u(1,i_u+1) - t_early_fast_down_tstar_u(1,i_u+1) + prob_up_tstar_u(1,i_u) / s_1_min);
        
        if t_early_fast_down_tstar_u(1,i_u) <= t_start || t_late_fast_down_tstar_u(1,i_u) >= t_end
            t_early_fast_down_tstar_u(1,i_u) = t_start;
            t_late_fast_down_tstar_u(1,i_u) = t_end;
            
            m_down_tstar(1) = i_u;
            s_1_remaining = s_1_tot - sum(prob_up_tstar_u(1,i_u+1:end), 2);
            remaining_m_down_tstar(1) = prob_up_tstar_u(1,i_u) - s_1_remaining;
        end
        
        interval.t_start = t_early_fast_down_tstar_u(1,i_u);
        interval.t_end = t_early_fast_down_tstar_u(1,i_u+1);
        interval.early = true;
        interval.i_tstar = 1;
        interval.i_u = i_u;
        fast_lane_intervals = [fast_lane_intervals; interval];
        
        interval.t_start = t_late_fast_down_tstar_u(1,i_u+1);
        interval.t_end = t_late_fast_down_tstar_u(1,i_u);
        interval.early = false;
        interval.i_tstar = 1;
        interval.i_u = i_u;
        fast_lane_intervals = [fast_lane_intervals; interval];
        
        if m_down_tstar(1) > 0
            break;
        end
    end
else
    intersection_found = false;
    
    for i_u = n_u : -1 : 1
        if intersection_found
            t_early_fast_down_tstar_u(1,i_u) = t_early_fast_down_tstar_u(1,i_u+1);
            t_late_fast_down_tstar_u(1,i_u) = t_late_fast_down_tstar_u(1,i_u+1) + prob_up_tstar_u(1,i_u) / s_1_min;
            
            t_early_fast_down_tstar_u(2,i_u) = t_early_fast_down_tstar_u(2,i_u+1) - prob_up_tstar_u(2,i_u) / s_1_min;
            t_late_fast_down_tstar_u(2,i_u) = t_late_fast_down_tstar_u(2,i_u+1);
        else
            for i_tstar = 1 : param.n_tstar
                t_early_fast_down_tstar_u(i_tstar,i_u) = param.tstar_min(i_tstar) - gamma_min / (beta_min + gamma_min) * (t_late_fast_down_tstar_u(i_tstar,i_u+1) - t_early_fast_down_tstar_u(i_tstar,i_u+1) + prob_up_tstar_u(i_tstar,i_u) / s_1_min);
                t_late_fast_down_tstar_u(i_tstar,i_u) = param.tstar_min(i_tstar) + beta_min / (beta_min + gamma_min) * (t_late_fast_down_tstar_u(i_tstar,i_u+1) - t_early_fast_down_tstar_u(i_tstar,i_u+1) + prob_up_tstar_u(i_tstar,i_u) / s_1_min);
            end
        end
        
        if ~intersection_found && t_early_fast_down_tstar_u(1,i_u) < t_late_fast_down_tstar_u(2,i_u)
            % First consider tstar1 'invading' tstar2
            u1 = U(i_u);
            for i_u2 = n_u : -1 : i_u
                if t_early_fast_down_tstar_u(1,i_u) < t_late_fast_down_tstar_u(2,i_u2)
                    u2 = U(i_u2);
                    t_inter_fast = (u2 * c_star_down_tstar(2) - u1 * c_star_down_tstar(1) + u1 * beta_min * param.tstar_min(1) + u2 * gamma_min * param.tstar_min(2)) / (u1 * beta_min + u2 * gamma_min);
                    if t_inter_fast >= t_early_fast_down_tstar_u(1,i_u) && t_inter_fast <= t_late_fast_down_tstar_u(2,i_u2)
                        t_late_fast_down_tstar_u(1,i_u) = t_late_fast_down_tstar_u(1,i_u) + t_inter_fast - t_early_fast_down_tstar_u(1,i_u);
                        t_early_fast_down_tstar_u(1,i_u) = t_inter_fast;
                        
                        t_early_fast_down_tstar_u(2,i_u2) = t_early_fast_down_tstar_u(2,i_u2) - (t_late_fast_down_tstar_u(2,i_u2) - t_inter_fast);
                        t_late_fast_down_tstar_u(2,i_u2) = t_inter_fast;
                        for i_u22 = i_u2 : -1 : i_u
                            t_early_fast_down_tstar_u(2,i_u22) = t_early_fast_down_tstar_u(2,i_u22+1) - prob_up_tstar_u(2,i_u22) / s_1_min;
                            t_late_fast_down_tstar_u(2,i_u22) = t_late_fast_down_tstar_u(2,i_u22+1);
                        end
                        
                        intersection_found = true;
                        break;
                    end
                end
            end
            % Next consider tstar2 'invading' tstar1
            if ~intersection_found
                u2 = U(i_u);
                for i_u1 = n_u : -1 : i_u
                    if t_early_fast_down_tstar_u(1,i_u1) < t_late_fast_down_tstar_u(2,i_u)
                        u1 = U(i_u1);
                        t_inter_fast = (u2 * c_star_down_tstar(2) - u1 * c_star_down_tstar(1) + u1 * beta_min * param.tstar_min(1) + u2 * gamma_min * param.tstar_min(2)) / (u1 * beta_min + u2 * gamma_min);
                        if t_inter_fast >= t_early_fast_down_tstar_u(1,i_u1) && t_inter_fast <= t_late_fast_down_tstar_u(2,i_u)
                            t_early_fast_down_tstar_u(2,i_u) = t_early_fast_down_tstar_u(2,i_u) - (t_late_fast_down_tstar_u(2,i_u) - t_inter_fast);
                            t_late_fast_down_tstar_u(2,i_u) = t_inter_fast;
                            
                            t_late_fast_down_tstar_u(1,i_u1) = t_late_fast_down_tstar_u(1,i_u1) + t_inter_fast - t_early_fast_down_tstar_u(1,i_u1);
                            t_early_fast_down_tstar_u(1,i_u1) = t_inter_fast;
                            for i_u12 = i_u1 - 1 : -1 : i_u
                                t_early_fast_down_tstar_u(1,i_u12) = t_early_fast_down_tstar_u(1,i_u12+1);
                                t_late_fast_down_tstar_u(1,i_u12) = t_late_fast_down_tstar_u(1,i_u12+1) + prob_up_tstar_u(1,i_u12) / s_1_min;
                            end

                            intersection_found = true;
                            break;
                        end
                    end
                end
            end
        end
    end
    
    for i_tstar = 1 : param.n_tstar
        for i_u = n_u : -1 : 1
            if t_early_fast_down_tstar_u(i_tstar,i_u) <= t_start || t_late_fast_down_tstar_u(i_tstar,i_u) >= t_end
                m_down_tstar(i_tstar) = i_u;
                
                if t_early_fast_down_tstar_u(i_tstar,i_u) < t_start
                    remaining_m_down_tstar(i_tstar) = remaining_m_down_tstar(i_tstar) + (t_start - t_early_fast_down_tstar_u(i_tstar,i_u)) * s_1_min;
                    t_early_fast_down_tstar_u(i_tstar,i_u) = t_start;
                end
                
                if t_late_fast_down_tstar_u(i_tstar,i_u) > t_end
                    remaining_m_down_tstar(i_tstar) = remaining_m_down_tstar(i_tstar) + (t_late_fast_down_tstar_u(i_tstar,i_u) - t_end) * s_1_min;
                    t_late_fast_down_tstar_u(i_tstar,i_u) = t_end;
                end

                break;
            end
        end
    end
end

r_analytical_down_tstar_u_t_o = zeros(param.n_tstar, n_u, n_t_analytical, sne_param.n_o);

% Fast lane departure rates
for i_interval = 1 : length(fast_lane_intervals)
    interval = fast_lane_intervals{i_interval};
    i_tstar = interval.i_tstar;
    i_u = interval.i_u;
    if interval.early
        if i_u == n_u
            i_t_analytical = find(t_analytical >= interval.t_start & t_analytical <= interval.t_end);
        else
            i_t_analytical = find(t_analytical >= interval.t_start & t_analytical < interval.t_end);
        end
    else
        i_t_analytical = find(t_analytical > interval.t_start & t_analytical <= interval.t_end);
    end
    r_analytical_down_tstar_u_t_o(i_tstar,i_u,i_t_analytical,1) = s_1_min;
end

% Slow lane departure rates
prob_up_tstar_slow = remaining_m_down_tstar;
for i_tstar = 1 : param.n_tstar
    prob_up_tstar_slow(i_tstar) = prob_up_tstar_slow(i_tstar) + sum(prob_up_tstar_u(i_tstar,1:m_down_tstar(i_tstar)-1), 2);
end
r_early = alpha_min / (alpha_min - beta_min) * s_2_min;
r_late = alpha_min / (alpha_min + gamma_min) * s_2_min;
if param.multi_peak
    t_peak2 = param.tstar_min(2) - beta_min / alpha_min * (param.tstar_min(2) - t_start);
    t_inter_slow = (prob_up_tstar_slow(2) - (r_early - r_late) * t_peak2 + r_early * t_start) / r_late;
    t_peak1 = (prob_up_tstar_slow(1) + r_early * t_inter_slow - r_late * t_end) / (r_early - r_late);
    
    for i_tstar = 1 : param.n_tstar
        if i_tstar == 1
            i_t_early_analytical = find(t_analytical >= t_inter_slow & t_analytical <= t_peak1);
            i_t_late_analytical = find(t_analytical > t_peak1 & t_analytical <= t_end);
        else
            i_t_early_analytical = find(t_analytical >= t_start & t_analytical <= t_peak2);
            i_t_late_analytical = find(t_analytical > t_peak2 & t_analytical < t_inter_slow);
        end
        
        m = m_down_tstar(i_tstar);
        for i_u = 1 : m - 1
            r_analytical_down_tstar_u_t_o(i_tstar,i_u,i_t_early_analytical,2) = prob_up_tstar_u(i_tstar,i_u) * s_min / s_2_min * r_early;
            r_analytical_down_tstar_u_t_o(i_tstar,i_u,i_t_late_analytical,2) = prob_up_tstar_u(i_tstar,i_u) * s_min / s_2_min * r_late;
        end
        r_analytical_down_tstar_u_t_o(i_tstar,m,i_t_early_analytical,2) = remaining_m_down_tstar(i_tstar) * s_min / s_2_min * r_early;
        r_analytical_down_tstar_u_t_o(i_tstar,m,i_t_late_analytical,2) = remaining_m_down_tstar(i_tstar) * s_min / s_2_min * r_late;
    end
else
    t_peak = param.tstar_min(1) - c_star / alpha_min;
    i_t_early_analytical = find(t_analytical >= t_start & t_analytical <= t_peak);
    i_t_late_analytical = find(t_analytical > t_peak & t_analytical <= t_end);
    
    m = m_down_tstar(1);
    for i_u = 1 : m - 1
        r_analytical_down_tstar_u_t_o(1,i_u,i_t_early_analytical,2) = prob_up_tstar_u(1,i_u) * s_min / s_2_min * r_early;
        r_analytical_down_tstar_u_t_o(1,i_u,i_t_late_analytical,2) = prob_up_tstar_u(1,i_u) * s_min / s_2_min * r_late;
    end
    r_analytical_down_tstar_u_t_o(1,m,i_t_early_analytical,2) = remaining_m_down_tstar(1) * s_min / s_2_min * r_early;
    r_analytical_down_tstar_u_t_o(1,m,i_t_late_analytical,2) = remaining_m_down_tstar(1) * s_min / s_2_min * r_late;
    
    if param.n_tstar > 1
        t_end2 = param.tstar_min(2) - beta_min / alpha_min * (param.tstar_min(2) - t_start);
        i_t_early_analytical = find(t_analytical >= t_start & t_analytical <= t_end2);
        
        m = m_down_tstar(2);
        for i_u = 1 : m - 1
            r_analytical_down_tstar_u_t_o(2,i_u,i_t_early_analytical,2) = prob_up_tstar_u(2,i_u) * s_min / s_2_min * r_early;
        end
        r_analytical_down_tstar_u_t_o(2,m,i_t_early_analytical,2) = remaining_m_down_tstar(2) * s_min / s_2_min * r_early;
    end
end


% Total departure rates per timestep in each lane
r_analytical_down_t_o = einsum('ijkl,ij->kl', r_analytical_down_tstar_u_t_o, ones(param.n_tstar, n_u));


%% Mass of queue per timestep in each lane
q_analytical_down_t_o = zeros(n_t_analytical, sne_param.n_o);

% Fast lane is at capacity
q_analytical_down_t_o(:,1) = 0;

% Slow lane obeys formula in Arott et al.
for i_t_analytical = 1 : n_t_analytical
    t = t_analytical(i_t_analytical);
    if param.multi_peak
        if t <= t_peak2
            q_analytical_down_t_o(i_t_analytical,2) = beta_min / (alpha_min - beta_min) * s_2_min * (t - t_start);
        elseif t <= t_inter_slow
            q_analytical_down_t_o(i_t_analytical,2) = beta_min / (alpha_min - beta_min) * s_2_min * (t_peak2 - t_start) ...
                - gamma_min / (alpha_min + gamma_min) * s_2_min * (t - t_peak2);
        elseif t <= t_peak1
            q_analytical_down_t_o(i_t_analytical,2) = beta_min / (alpha_min - beta_min) * s_2_min * (t_peak2 - t_start) ...
                - gamma_min / (alpha_min + gamma_min) * s_2_min * (t_inter_slow - t_peak2) ...
                + beta_min / (alpha_min - beta_min) * s_2_min * (t - t_inter_slow);
        else
            q_analytical_down_t_o(i_t_analytical,2) = beta_min / (alpha_min - beta_min) * s_2_min * (t_peak2 - t_start) ...
                - gamma_min / (alpha_min + gamma_min) * s_2_min * (t_inter_slow - t_peak2) ...
                + beta_min / (alpha_min - beta_min) * s_2_min * (t_peak1 - t_inter_slow) ...
                - gamma_min / (alpha_min + gamma_min) * s_2_min * (t - t_peak1);
        end
    else
        if t <= t_peak
            q_analytical_down_t_o(i_t_analytical,2) = beta_min / (alpha_min - beta_min) * s_2_min * (t - t_start);
        else
            q_analytical_down_t_o(i_t_analytical,2) = beta_min / (alpha_min - beta_min) * s_2_min * (t_peak - t_start) ...
                - gamma_min / (alpha_min + gamma_min) * s_2_min * (t - t_peak);
        end
    end
end

% Waiting time is straightforward from queue
t_q_analytical_down_t_o = zeros(n_t_analytical, sne_param.n_o);
t_q_analytical_down_t_o(:,1) = q_analytical_down_t_o(:,1) / s_1_min;
t_q_analytical_down_t_o(:,2) = q_analytical_down_t_o(:,2) / s_2_min;

t_q_analytical_down_t_o(isnan(t_q_analytical_down_t_o)) = 0;

%% Equilibrium optimal tolling price
p_analytical_down_t_o = zeros(n_t_analytical, sne_param.n_o);
curr_fast_lane_intervals = fast_lane_intervals;
curr_t_start = 0;
curr_p = 0;
while ~isempty(curr_fast_lane_intervals)
    for i_interval = 1 : length(curr_fast_lane_intervals)
        if curr_fast_lane_intervals{i_interval}.t_start == curr_t_start
            interval = curr_fast_lane_intervals{i_interval};
            i_tstar = interval.i_tstar;
            i_u = interval.i_u;
            u = U(i_u);
            
            if interval.early
                if i_u == n_u
                    i_t_analytical = find(t_analytical >= curr_t_start & t_analytical <= interval.t_end);
                else
                    i_t_analytical = find(t_analytical >= curr_t_start & t_analytical < interval.t_end);
                end
                
                p_analytical_down_t_o(i_t_analytical,1) = curr_p + u * beta_min * (t_analytical(i_t_analytical) - curr_t_start);
                
                curr_p = curr_p + u * beta_min * (interval.t_end - curr_t_start);
            else
                i_t_analytical = find(t_analytical > curr_t_start & t_analytical <= interval.t_end);
                
                p_analytical_down_t_o(i_t_analytical,1) = curr_p - u * gamma_min * (t_analytical(i_t_analytical) - curr_t_start);
                
                curr_p = curr_p - u * gamma_min * (interval.t_end - curr_t_start);
            end
            
            curr_t_start = interval.t_end;
            curr_fast_lane_intervals(i_interval) = [];
            
            break;
        end
    end
end

%% Equilibrium average waiting time per t* and urgency group
t_q_bar_analytical_down_tstar_u = zeros(param.n_tstar, n_u);

for i_tstar = 1 : param.n_tstar
    m = m_down_tstar(i_tstar);
    
    % Travellers going in fast lane incur no queuing delay
    t_q_bar_analytical_down_tstar_u(i_tstar,m+1:end) = 0;
    
    if param.multi_peak
        if i_tstar == 1
            curr_t_q_bar_slow = (beta_min / s_min ...
                - s_min * (beta_min + gamma_min) * (param.tstar_min(1) - param.tstar_min(2) + (beta_min - gamma_min) / ((beta_min + gamma_min) * 2 *s_min))^2)...
                / (4 * alpha_min);
        else
            curr_t_q_bar_slow = (gamma_min / s_min ...
                - s_min * (beta_min + gamma_min) * (param.tstar_min(1) - param.tstar_min(2) - (beta_min - gamma_min) / ((beta_min + gamma_min) * 2 *s_min))^2)...
                / (4 * alpha_min);
        end
    else
        if param.n_tstar > 1
            if i_tstar == 1
                curr_t_q_bar_slow = (c_star + prob_up_tstar_slow(1) / prob_up_tstar_slow(2) * beta_min * (param.tstar_min(1) - param.tstar_min(2))) / (2 * alpha_min);
            else
                curr_t_q_bar_slow = c_star_down_tstar(2) / (2 * alpha_min);
            end
        else
            curr_t_q_bar_slow = c_star / (2 * alpha_min);
        end
    end
    
    t_q_bar_analytical_down_tstar_u(i_tstar,1:m-1) = curr_t_q_bar_slow;
    t_q_bar_analytical_down_tstar_u(i_tstar,m) = remaining_m_down_tstar(i_tstar) / prob_up_tstar_u(i_tstar,m) * curr_t_q_bar_slow;
end


%% Equilibrium cost per urgency group (not counting monetary price)
c_bar_analytical_down_tstar_u = zeros(param.n_tstar, n_u);

% Slow lane costs
for i_tstar = 1 : param.n_tstar
    m = m_down_tstar(i_tstar);
    
    for i_u = 1 : m - 1
        c_bar_analytical_down_tstar_u(i_tstar,i_u) = U(i_u) * c_star_down_tstar(i_tstar);
    end
    
    c_bar_analytical_down_tstar_u(i_tstar,m) = U(m) * remaining_m_down_tstar(i_tstar) / prob_up_tstar_u(i_tstar,m) * c_star_down_tstar(i_tstar);
end

% Fast lane costs
for i_interval = 1 : length(fast_lane_intervals)
    interval = fast_lane_intervals{i_interval};
    i_tstar = interval.i_tstar;
    i_u = interval.i_u;
    u = U(i_u);
    t_mean = (interval.t_start + interval.t_end) / 2;
    t_prob = (interval.t_end - interval.t_start) * s_1_min / prob_up_tstar_u(i_tstar,i_u);
    if interval.early
        c_bar_analytical_down_tstar_u(i_tstar,i_u) = c_bar_analytical_down_tstar_u(i_tstar,i_u) + ...
            u * t_prob * beta_min * (param.tstar_min(i_tstar) - t_mean);
    else
        c_bar_analytical_down_tstar_u(i_tstar,i_u) = c_bar_analytical_down_tstar_u(i_tstar,i_u) + ...
            u * t_prob * gamma_min * (t_mean - param.tstar_min(i_tstar));
    end
end

end

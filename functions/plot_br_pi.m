% Plot SNE policy
function plot_br_pi(fg, position, colormap, br_pi_down_tstar_mu_alpha_u_k_up_t_b, param, sne_param, b_star)
    persistent br_pi_plot
    if ~ishandle(fg)
        figure(fg);
        fig = gcf;
        fig.Position = position;
        br_pi_plot = cell(param.n_tstar, param.n_mu, param.n_alpha, param.n_u, param.T);
        n_rows = param.n_tstar * param.n_mu * param.n_alpha * param.n_u;
        n_cols = param.T;
        i_subplot = 1;
        for i_tstar = 1 : param.n_tstar
            for i_mu = 1 : param.n_mu
                for i_alpha = 1 : param.n_alpha
                    for i_u = 1 : param.n_u
                        for t = 1 : param.T
                            pi_mat = squeeze(br_pi_down_tstar_mu_alpha_u_k_up_t_b(i_tstar,i_mu,i_alpha,i_u,:,t,:));
                            for i_k = 1 : sne_param.n_k - 1
                                pi_mat(i_k,i_k+1:sne_param.n_k) = nan;
                            end

                            subplot(n_rows, n_cols, i_subplot);
                            br_pi_plot{i_tstar,i_mu,i_alpha,i_u,t} = heatmap(sne_param.K, sne_param.K, pi_mat.', 'ColorbarVisible','off', 'MissingDataColor', [0.8275 0.8275 0.8275]);    % disallowed bids displayed gray
                            br_pi_plot{i_tstar,i_mu,i_alpha,i_u,t}.YDisplayData = flipud(br_pi_plot{i_tstar,i_mu,i_alpha,i_u,t}.YDisplayData);
                            
                            title_str = ['t*=', num2str(param.tstar(i_tstar))];
                            if param.n_mu > 1
                                title_str = [title_str, ' \mu=', num2str(i_mu)'];
                            end
                            if param.n_alpha > 1
                                alpha = param.Alpha(i_alpha);
                                if alpha > 0.99 && alpha < 1
                                    alpha_str = num2str(alpha, '%.3f');
                                else
                                    alpha_str = num2str(alpha, '%.2f');
                                end
                                title_str = [title_str, ' \alpha=', alpha_str];
                            end
                            br_pi_plot{i_tstar,i_mu,i_alpha,i_u,t}.Title = [title_str, ' u=', num2str(param.U_down_mu(i_mu,i_u)), ' t=', num2str(t), ' b*=', num2str(b_star(t))];
                            
                            br_pi_plot{i_tstar,i_mu,i_alpha,i_u,t}.XLabel = 'Karma';
                            br_pi_plot{i_tstar,i_mu,i_alpha,i_u,t}.YLabel = 'Bid';
                            
                            br_pi_plot{i_tstar,i_mu,i_alpha,i_u,t}.FontName = 'ubuntu';
                            br_pi_plot{i_tstar,i_mu,i_alpha,i_u,t}.FontSize = sne_param.plot_font_size;
                            
                            br_pi_plot{i_tstar,i_mu,i_alpha,i_u,t}.Colormap = colormap;
                            br_pi_plot{i_tstar,i_mu,i_alpha,i_u,t}.ColorLimits = [0 1];
                            br_pi_plot{i_tstar,i_mu,i_alpha,i_u,t}.CellLabelColor = 'none';
                            
                            br_pi_plot{i_tstar,i_mu,i_alpha,i_u,t}.GridVisible = false;
                            
                            for i_k = 1 : sne_param.n_k
                                if mod(i_k - 1, 5) ~= 0
                                    br_pi_plot{i_tstar,i_mu,i_alpha,i_u,t}.XDisplayLabels{i_k} = '';
                                    br_pi_plot{i_tstar,i_mu,i_alpha,i_u,t}.YDisplayLabels{i_k} = '';
                                end
                            end

                            i_subplot = i_subplot + 1;
                        end
                    end
                end
            end
        end
        sgtitle('BR policy', 'FontSize', 20, 'FontName', 'ubunutu', 'FontWeight', 'bold');
    else
        for i_tstar = 1 : param.n_tstar
            for i_mu = 1 : param.n_mu
                for i_alpha = 1 : param.n_alpha
                    for i_u = 1 : param.n_u
                        for t = 1 : param.T
                            pi_mat = squeeze(br_pi_down_tstar_mu_alpha_u_k_up_t_b(i_tstar,i_mu,i_alpha,i_u,:,t,:));
                            for i_k = 1 : sne_param.n_k - 1
                                pi_mat(i_k,i_k+1:sne_param.n_k) = nan;
                            end
                            br_pi_plot{i_tstar,i_mu,i_alpha,i_u,t}.ColorData = pi_mat.';
                            
                            title_str = ['t*=', num2str(param.tstar(i_tstar))];
                            if param.n_mu > 1
                                title_str = [title_str, ' \mu=', num2str(i_mu)'];
                            end
                            if param.n_alpha > 1
                                alpha = param.Alpha(i_alpha);
                                if alpha > 0.99 && alpha < 1
                                    alpha_str = num2str(alpha, '%.3f');
                                else
                                    alpha_str = num2str(alpha, '%.2f');
                                end
                                title_str = [title_str, ' \alpha=', alpha_str];
                            end
                            br_pi_plot{i_tstar,i_mu,i_alpha,i_u,t}.Title = [title_str, ' u=', num2str(param.U_down_mu(i_mu,i_u)), ' t=', num2str(t), ' b*=', num2str(b_star(t))];
                        end
                    end
                end
            end
        end
    end
end
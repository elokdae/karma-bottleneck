% Plot SNE value function
function plot_sne_V(fg, position, sne_V_down_t_mu_alpha_u_k, param, sne_param)
    persistent sne_V_plot
    if ~ishandle(fg)
        figure(fg);
        fig = gcf;
        fig.Position = position;
        sne_V_plot = cell(param.T, param.n_mu, param.n_alpha, param.n_u);
        n_rows = param.n_tstar * param.n_mu;
        n_cols = param.n_alpha;
        lgd_text = cell(param.n_u, 1);
        i_subplot = 1;
        for i_tstar = 1 : param.n_tstar
            for i_mu = 1 : param.n_mu
                for i_alpha = 1 : param.n_alpha
                    subplot(n_rows, n_cols, i_subplot);
                    hold on;
                    for i_u = 1 : param.n_u
                        sne_V_plot{i_tstar,i_mu,i_alpha,i_u} = plot(sne_param.K, squeeze(sne_V_down_t_mu_alpha_u_k(i_tstar,i_mu,i_alpha,i_u,:)), '-x', 'LineWidth', 2);

                        lgd_text{i_u} = ['u=', num2str(param.U_down_mu(i_mu,i_u))];
                    end
                    axis tight;
                    axes = gca;
                    
                    title_str = ['t*=', num2str(param.tstar(i_tstar))];
                    if param.n_mu > 1
                        title_str = [title_str, ' \mu=', num2str(i_mu)'];
                    end
                    if param.n_alpha > 1
                        alpha = param.Alpha(i_alpha);
                        if alpha > 0.99 && alpha < 1
                            alpha_str = num2str(alpha, '%.3f');
                        else
                            alpha_str = num2str(alpha, '%.2f');
                        end
                        title_str = [title_str, ' \alpha=', alpha_str];
                    end
                    axes.Title.String = title_str;
                    
                    axes.XLabel.String = 'Karma';
                    axes.YLabel.String = 'Value';
                    
                    lgd = legend(lgd_text);
                    lgd.Location = 'bestoutside';
                    
                    axes.Title.FontName = 'ubuntu';
                    axes.XLabel.FontName = 'ubuntu';
                    axes.YLabel.FontName = 'ubuntu';
                    axes.XAxis.FontName = 'ubuntu';
                    axes.YAxis.FontName = 'ubuntu';
                    lgd.FontName = 'ubuntu';

                    axes.Title.FontSize = sne_param.plot_font_size;
                    axes.XLabel.FontSize = sne_param.plot_font_size;
                    axes.YLabel.FontSize = sne_param.plot_font_size;
                    axes.XAxis.FontSize = sne_param.plot_font_size;
                    axes.YAxis.FontSize = sne_param.plot_font_size;
                    lgd.FontSize = sne_param.plot_font_size;
                    
                    i_subplot = i_subplot + 1;
                end
            end
        end
        sgtitle('Value function', 'FontSize', 20, 'FontName', 'ubunutu', 'FontWeight', 'bold');
    else
        for i_tstar = 1 : param.n_tstar
            for i_mu = 1 : param.n_mu
                for i_alpha = 1 : param.n_alpha
                    for i_u = 1 : param.n_u
                        sne_V_plot{i_tstar,i_mu,i_alpha,i_u}.YData = squeeze(sne_V_down_t_mu_alpha_u_k(i_tstar,i_mu,i_alpha,i_u,:));
                    end
                    
                    title_str = ['t*=', num2str(param.tstar(i_tstar))];
                    if param.n_mu > 1
                        title_str = [title_str, ' \mu=', num2str(i_mu)'];
                    end
                    if param.n_alpha > 1
                        alpha = param.Alpha(i_alpha);
                        if alpha > 0.99 && alpha < 1
                            alpha_str = num2str(alpha, '%.3f');
                        else
                            alpha_str = num2str(alpha, '%.2f');
                        end
                        title_str = [title_str, ' \alpha=', alpha_str];
                    end
                    sne_V_plot{i_tstar,i_mu,i_alpha,1}.Parent.Title.String = title_str;
                end
            end
        end
    end
end
% Plot SNE type-state distribution
function plot_sne_d(fg, position, sne_d_up_tstar_mu_alpha_u_k, param, sne_param)
    persistent sne_d_plot
    if ~ishandle(fg)
        figure(fg);
        fig = gcf;
        fig.Position = position;
        sne_d_plot = cell(param.T, param.n_mu, param.n_alpha, param.n_u);
        d_max = max(sne_d_up_tstar_mu_alpha_u_k(:));
        n_rows = param.n_tstar * param.n_mu * param.n_alpha;
        n_cols = param.n_u;
        i_subplot = 1;
        for i_tstar = 1 : param.n_tstar
            for i_mu = 1 : param.n_mu
                for i_alpha = 1 : param.n_alpha
                    for i_u = 1 : param.n_u
                        subplot(n_rows, n_cols, i_subplot);
                        sne_d_plot{i_tstar,i_mu,i_alpha,i_u} = bar(sne_param.K, squeeze(sne_d_up_tstar_mu_alpha_u_k(i_tstar,i_mu,i_alpha,i_u,:)));
                        
                        axis tight;
                        axes = gca;
                        
                        axes.YLim = [0, d_max];
                        
                        title_str = ['t*=', num2str(param.tstar(i_tstar))];
                        if param.n_mu > 1
                            title_str = [title_str, ' \mu=', num2str(i_mu)'];
                        end
                        if param.n_alpha > 1
                            alpha = param.Alpha(i_alpha);
                            if alpha > 0.99 && alpha < 1
                                alpha_str = num2str(alpha, '%.3f');
                            else
                                alpha_str = num2str(alpha, '%.2f');
                            end
                            title_str = [title_str, ' \alpha=', alpha_str];
                        end
                        axes.Title.String = [title_str, ' u=', num2str(param.U_down_mu(i_mu,i_u))];
                        
                        axes.XLabel.String = 'Karma';
                        axes.YLabel.String = 'Distribution';
                        
                        axes.Title.FontName = 'ubuntu';
                        axes.XLabel.FontName = 'ubuntu';
                        axes.YLabel.FontName = 'ubuntu';
                        axes.XAxis.FontName = 'ubuntu';
                        axes.YAxis.FontName = 'ubuntu';
                        
                        axes.Title.FontSize = sne_param.plot_font_size;
                        axes.XLabel.FontSize = sne_param.plot_font_size;
                        axes.YLabel.FontSize = sne_param.plot_font_size;
                        axes.XAxis.FontSize = sne_param.plot_font_size;
                        axes.YAxis.FontSize = sne_param.plot_font_size;
                        
                        i_subplot = i_subplot + 1;
                    end
                end
            end
        end
        sgtitle('SNE distribution', 'FontSize', 20, 'FontName', 'ubunutu', 'FontWeight', 'bold');
    else
        d_max = max(sne_d_up_tstar_mu_alpha_u_k(:));
        for i_tstar = 1 : param.n_tstar
            for i_mu = 1 : param.n_mu
                for i_alpha = 1 : param.n_alpha
                    for i_u = 1 : param.n_u
                        sne_d_plot{i_tstar,i_mu,i_alpha,i_u}.YData = sne_d_up_tstar_mu_alpha_u_k(i_tstar,i_mu,i_alpha,i_u,:);
                        
                        sne_d_plot{i_tstar,i_mu,i_alpha,i_u}.Parent.YLim = [0, d_max];
                        
                        title_str = ['t*=', num2str(param.tstar(i_tstar))];
                        if param.n_mu > 1
                            title_str = [title_str, ' \mu=', num2str(i_mu)'];
                        end
                        if param.n_alpha > 1
                            alpha = param.Alpha(i_alpha);
                            if alpha > 0.99 && alpha < 1
                                alpha_str = num2str(alpha, '%.3f');
                            else
                                alpha_str = num2str(alpha, '%.2f');
                            end
                            title_str = [title_str, ' \alpha=', alpha_str];
                        end
                        sne_d_plot{i_tstar,i_mu,i_alpha,i_u}.Parent.Title.String = [title_str, ' u=', num2str(param.U_down_mu(i_mu,i_u))];
                    end
                end
            end
        end
    end
end
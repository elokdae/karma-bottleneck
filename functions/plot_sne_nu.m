% Plot SNE karma distribution
function plot_sne_nu(fg, position, sne_nu_up_t_bj, param, sne_param, b_star)
    persistent sne_nu_plot b_star_plot b_star_text
    
    nu_max = max(sne_nu_up_t_bj(:));
    b_star_max = max(b_star);

    if ~ishandle(fg)
        figure(fg);
        fig = gcf;
        fig.Position = position;
        sne_nu_plot = cell(param.T, 1);
        b_star_plot = cell(param.T, 1);
        b_star_text = cell(param.T, 1);
        
        
        
        n_rows = 1;
        n_cols = param.T;
        i_subplot = 1;
        for t = 1 : param.T
            subplot(n_rows, n_cols, i_subplot);
            sne_nu_plot{t} = bar(0:min([b_star_max+2,sne_param.n_k-1]), sne_nu_up_t_bj(t,1:min([b_star_max+3,sne_param.n_k])));
            axis tight;
            axes = gca;
            
            axes.YLim = [0, nu_max];
            
            axes.Title.String = ['t=', num2str(t)];
            axes.XLabel.String = 'Bid';
            axes.YLabel.String = 'Distribution';
            
            axes.Title.FontName = 'ubuntu';
            axes.XLabel.FontName = 'ubuntu';
            axes.YLabel.FontName = 'ubuntu';
            axes.XAxis.FontName = 'ubuntu';
            axes.YAxis.FontName = 'ubuntu';

            axes.Title.FontSize = sne_param.plot_font_size;
            axes.XLabel.FontSize = sne_param.plot_font_size;
            axes.YLabel.FontSize = sne_param.plot_font_size;
            axes.XAxis.FontSize = sne_param.plot_font_size;
            axes.YAxis.FontSize = sne_param.plot_font_size;
            
            hold on;
            b_star_plot{t} = stem(b_star(t), nu_max, 'Marker', 'none', 'LineWidth', 2);
            b_star_text{t} = text(b_star(t) + 0.5, 0.95 * nu_max, ['b*=', num2str(b_star(t))], 'FontSize', sne_param.plot_font_size, 'FontWeight', 'bold', 'FontName', 'ubuntu', 'Color', [0.8500 0.3250 0.0980]);

            i_subplot = i_subplot + 1;
        end
        sgtitle('Bid distribution', 'FontSize', 20, 'FontName', 'ubunutu', 'FontWeight', 'bold');
    else
        for t = 1 : param.T
            sne_nu_plot{t}.XData = 0:min([b_star_max+2,sne_param.n_k-1]);
            sne_nu_plot{t}.YData = sne_nu_up_t_bj(t,1:min([b_star_max+3,sne_param.n_k]));
            
            sne_nu_plot{t}.Parent.Title.String = ['t=', num2str(t)];
            sne_nu_plot{t}.Parent.YLim = [0, nu_max];
            
            b_star_plot{t}.XData = b_star(t);
            b_star_plot{t}.YData = nu_max;
            
            b_star_text{t}.Position(1) = b_star(t) + 0.5;
            b_star_text{t}.Position(2) = 0.95 * nu_max;
            b_star_text{t}.String = ['b*=', num2str(b_star(t))];
        end
    end
end
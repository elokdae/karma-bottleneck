% Plot policy and distribution error to equilibrium
function plot_sne_pi_d_error_to_eq(fg, position, sne_pi_error_to_eq_hist, sne_d_error_t_eq_hist, sne_param)
    persistent sne_pi_d_error_to_eq_plot
    if ~ishandle(fg)
        figure(fg);
        fig = gcf;
        fig.Position = position;
        sne_pi_d_error_to_eq_plot = cell(2, 1);
        sne_pi_d_error_to_eq_plot{1} = plot(sne_pi_error_to_eq_hist, 'LineWidth', 2);
        hold on;
        sne_pi_d_error_to_eq_plot{2} = plot(sne_d_error_t_eq_hist, 'LineWidth', 2);
        axis tight;
        axes = gca;
        
        axes.Title.String = 'Policy and distribution error to SNE';
        axes.XLabel.String = 'Iteration';
        axes.YLabel.String = 'Error';
        
        lgd = legend(["Policy error", "Distribution error"]);
        lgd.Location = 'bestoutside';
        
        axes.Title.FontName = 'ubuntu';
        axes.XLabel.FontName = 'ubuntu';
        axes.YLabel.FontName = 'ubuntu';
        axes.XAxis.FontName = 'ubuntu';
        axes.YAxis.FontName = 'ubuntu';
        lgd.FontName = 'ubuntu';

        axes.Title.FontSize = 20;
        axes.XLabel.FontSize = sne_param.plot_font_size;
        axes.YLabel.FontSize = sne_param.plot_font_size;
        axes.XAxis.FontSize = sne_param.plot_font_size;
        axes.YAxis.FontSize = sne_param.plot_font_size;
        lgd.FontSize = sne_param.plot_font_size;
    else
        sne_pi_d_error_to_eq_plot{1}.YData = sne_pi_error_to_eq_hist;
        sne_pi_d_error_to_eq_plot{2}.YData = sne_d_error_t_eq_hist;
    end
end
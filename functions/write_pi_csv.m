% Write policy to csv file
function write_pi_csv(pi_down_tstar_mu_alpha_u_k_up_t_b, k_max, b_star, zero_tol, param, sne_param, fileprefix)
    n_k = k_max + 1;
%     b_star_max = max(b_star);
%     n_b = b_star_max + 3;
    n_b = n_k;
    
    pi_down_tstar_mu_alpha_u_k_up_t_b(pi_down_tstar_mu_alpha_u_k_up_t_b <= zero_tol) = -1;
    
    % Header
    header = ["tstar", "mu", "alpha", "i-u", "u", "k", "k2", "t", "t-min", "b", "b2", "P(b)", "b-star"];
    filename = [fileprefix, '-policy.csv'];
    fout = fopen(filename, 'w');
    for i = 1 : length(header) - 1
        fprintf(fout, '%s,', header(i));
    end
    fprintf(fout, '%s\n', header(end));
    fclose(fout);

    % Header for mean of policy
    header_mean = ["tstar", "mu", "alpha", "i-u", "u", "k", "t", "t-min", "b", "b-star"];
    filename_mean = [fileprefix, '-policy-mean.csv'];
    fout = fopen(filename_mean, 'w');
    for i = 1 : length(header_mean) - 1
        fprintf(fout, '%s,', header_mean(i));
    end
    fprintf(fout, '%s\n', header_mean(end));
    fclose(fout);

    % Data
    for i_tstar = 1 : param.n_tstar
        tstar = param.tstar(i_tstar);
        for i_mu = 1 : param.n_mu
            for i_alpha = 1 : param.n_alpha
                alpha = param.Alpha(i_alpha);
                for i_u = 1 : param.n_u
                    u = param.U_down_mu(i_mu,i_u);
                    for i_k = 1 : n_k + 1
                        k = i_k - 1;
                        for t = 1 : param.T
                            for i_b = 1 : n_b + 1
                                b = i_b - 1;
                                if i_b <= i_k && i_k <= n_k
                                    line = [tstar, i_mu, alpha, i_u, u, k, k - 0.5, t, param.t_min(t), b, b - 0.5, pi_down_tstar_mu_alpha_u_k_up_t_b(i_tstar,i_mu,i_alpha,i_u,i_k,t,i_b), b_star(t)];
                                else
                                    line = [tstar, i_mu, alpha, i_u, u, k, k - 0.5, t, param.t_min(t), b, b - 0.5, 2, b_star(t)];
                                end
                                dlmwrite(filename, line, '-append');
                            end
                            
                            if i_k <= n_k
                                line_mean = [tstar, i_mu, alpha, i_u, u, k, t, param.t_min(t), dot(squeeze(pi_down_tstar_mu_alpha_u_k_up_t_b(i_tstar,i_mu,i_alpha,i_u,i_k,t,:)), sne_param.K), b_star(t)];
                                dlmwrite(filename_mean, line_mean, '-append');
                            end
                        end
                    end
                end
            end
        end
    end
end
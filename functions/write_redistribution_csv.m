% Write b_star to csv file
function write_redistribution_csv(redistribution, param, fileprefix)
    % Header for b_star
    header = ["t", "t-min", "r"];
    filename = [fileprefix, '-redistribution.csv'];
    fout = fopen(filename, 'w');
    for i = 1 : length(header) - 1
        fprintf(fout, '%s,', header(i));
    end
    fprintf(fout, '%s\n', header(end));
    fclose(fout);
    
    % Data for redistribution
    data = [param.t, param.t_min, redistribution];
    dlmwrite(filename, data, '-append');
end
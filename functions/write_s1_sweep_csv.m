% Write s1 sweep to csv file
function write_s1_sweep_csv(s1_sweep, t_q_bar_min, t_q_bar_processed_min, c_bar, b_star_tstar, t_q_bar_min_toll, c_bar_toll, p_star_tstar, t_q_bar_min_nom, c_bar_nom, u_bar, param, fileprefix)
    % Header
    header = ["s1/s", "t-q-bar-min", "t-q-bar-processed-min", "c-bar", "c-bar-norm", "b-star", "t-q-bar-min-toll", "c-bar-toll", "c-bar-toll-norm", "p-star", "t-q-bar-min-nom", "c-bar-nom", "c-bar-nom-norm", "u-bar"];
    filename = [fileprefix, '-s1-sweep.csv'];
    fout = fopen(filename, 'w');
    for i = 1 : length(header) - 1
        fprintf(fout, '%s,', header(i));
    end
    fprintf(fout, '%s\n', header(end));
    fclose(fout);
    
    % Data for b-star
    n_s1_sweep = length(s1_sweep);
    data = [s1_sweep, t_q_bar_min, t_q_bar_processed_min, c_bar, c_bar / u_bar, b_star_tstar,...
        t_q_bar_min_toll, c_bar_toll, c_bar_toll / u_bar, p_star_tstar,...
        t_q_bar_min_nom * ones(n_s1_sweep, 1), c_bar_nom * ones(n_s1_sweep, 1), c_bar_nom / u_bar * ones(n_s1_sweep, 1),...
        u_bar * ones(n_s1_sweep, 1)];
    dlmwrite(filename, data, '-append');
end
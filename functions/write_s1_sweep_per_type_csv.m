% Write s1 sweep to csv file
function write_s1_sweep_per_type_csv(s1_sweep, t_q_bar_min_down_mu, t_q_bar_processed_min_down_mu, c_bar_down_mu, b_star_tstar, t_q_bar_min_toll_down_mu, c_bar_toll_down_mu, p_star_tstar, t_q_bar_min_nom_down_mu, c_bar_nom_down_mu, u_bar_down_mu, param, fileprefix)
    % Header
    header = ["s1/s", "mu", "t-q-bar-min", "t-q-bar-processed-min", "c-bar", "c-bar-norm", "b-star", "t-q-bar-min-toll", "c-bar-toll", "c-bar-toll-norm", "p-star", "t-q-bar-min-nom", "c-bar-nom", "c-bar-nom-norm", "u-bar"];
    filename = [fileprefix, '-s1-sweep-per-type.csv'];
    fout = fopen(filename, 'w');
    for i = 1 : length(header) - 1
        fprintf(fout, '%s,', header(i));
    end
    fprintf(fout, '%s\n', header(end));
    fclose(fout);
    
    % Data
    n_s1_sweep = length(s1_sweep);
    mu_vec = (1 : param.n_mu).';
    for i_s1 = 1 : n_s1_sweep
        data = [s1_sweep(i_s1) * ones(param.n_mu, 1), mu_vec, t_q_bar_min_down_mu(i_s1,:).', t_q_bar_processed_min_down_mu(i_s1,:).', c_bar_down_mu(i_s1,:).', c_bar_down_mu(i_s1,:).' ./ u_bar_down_mu.', b_star_tstar(i_s1) * ones(param.n_mu, 1),...
            t_q_bar_min_toll_down_mu(i_s1,:).', c_bar_toll_down_mu(i_s1,:).', c_bar_toll_down_mu(i_s1,:).' ./ u_bar_down_mu.', p_star_tstar(i_s1) * ones(param.n_mu, 1),...
            t_q_bar_min_nom_down_mu.', c_bar_nom_down_mu.', c_bar_nom_down_mu.' ./ u_bar_down_mu.',...
            u_bar_down_mu.'];
        dlmwrite(filename, data, '-append');
    end
end
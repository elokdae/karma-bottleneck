clc;
screenwidth = 1920;
screenheight = 1080;

%% Some parameters
s1_sweep = [0.05 : 0.05 : 0.95].';
n_s1_sweep = length(s1_sweep);

file_start = 'results/baseline-karma/k_bar-10-alpha-0.99-s1-';

%% Performance measure arrays
t_q_bar_min = zeros(n_s1_sweep, 1);
t_q_bar_processed_min = zeros(n_s1_sweep, 1);
c_bar = zeros(n_s1_sweep, 1);
b_star_tstar = zeros(n_s1_sweep, 1);

t_q_bar_min_toll = zeros(n_s1_sweep, 1);
c_bar_toll = zeros(n_s1_sweep, 1);
p_star_tstar = zeros(n_s1_sweep, 1);

for i_s1 = 1 : n_s1_sweep
    s1 = s1_sweep(i_s1);
    file = [file_start, num2str(s1, '%.2f'), '.mat'];
    if exist(file, 'file')
        load(file);

        % average queue delays
        t_q_bar_min(i_s1) = param.dt * t_q_bar;
        
        % average queue delays processed
        t_q_processed_down_t_o = t_q_down_t_o;
        for t = 2 : param.T
            t_q_processed_down_t_o(t,2) = mean(squeeze(t_q_down_t_o(t-1:t,2)));
        end
        t_q_bar_processed = einsum('ijk,ijk->k', n_down_t_o, t_q_processed_down_t_o);
        t_q_bar_processed_min(i_s1) = param.dt * t_q_bar_processed;
        
        % average travel cost
        c_bar(i_s1) = -r_bar;
        
        % b_star of tstar
        b_star_tstar(i_s1) = b_star(param.tstar);
        
        % toll average queue delays
        t_q_bar_min_toll(i_s1) = einsum('ij,ijl->l', prob_up_tstar_u, t_q_bar_analytical_down_tstar_u);
        
        % toll average travel cost
        c_bar_toll(i_s1) = einsum('ij,ijl->l', prob_up_tstar_u, c_bar_analytical_down_tstar_u);
        
        % toll p_star of tstar
        i_tstar = find(t_analytical >= param.tstar_min);
        i_tstar = i_tstar(1);
        p_star_tstar(i_s1) = p_analytical_down_t_o(i_tstar, 1);
    else
        t_q_bar_min(i_s1) = nan;
        t_q_bar_processed_min(i_s1) = nan;
        c_bar(i_s1) = nan;
        t_q_bar_min_toll(i_s1) = nan;
        c_bar_toll(i_s1) = nan;
    end
end


%% Performance measures for nominal scheme (no lane split)
c_star = param.beta * param.gamma / ((param.beta + param.gamma) * param.s);
t_q_bar_min_nom = param.dt * c_star / (2 * param.alpha);
u_bar = einsum('ik,ij,ij->k', param.g_up_mu, param.prob_down_mu_up_u, param.U_down_mu);
c_bar_nom = u_bar * c_star;

%% Plot
figure(1);
fig = gcf;
fig.Position = [0, 0, screenwidth, screenheight];
% % Average queuing delay
% subplot(1,3,1);
% plot(s1_sweep, t_q_bar_min_nom * ones(n_s1_sweep, 1), 'LineWidth', 2);
% hold on;
% plot(s1_sweep, t_q_bar_min_toll, 'LineWidth', 2);
% plot(s1_sweep, t_q_bar_min, '--o', 'LineWidth', 2, 'MarkerSize', 10);
% axis tight;
% axes = gca;
% axes.XAxis.FontSize = 15;
% axes.XAxis.FontName = 'ubuntu';
% axes.YAxis.FontSize = 15;
% axes.YAxis.FontName = 'ubuntu';
% axes.XLabel.FontName = 'ubuntu';
% axes.XLabel.FontWeight = 'bold';
% axes.XLabel.String = 'Fraction of fast lane capacity';
% axes.XLabel.FontSize = 20;
% axes.YLabel.FontName = 'ubuntu';
% axes.YLabel.FontWeight = 'bold';
% axes.YLabel.String = 'Average queuing delay';
% axes.YLabel.FontSize = 20;
% Average queuing delay processed
subplot(1,3,1);
plot(s1_sweep, t_q_bar_min_nom * ones(n_s1_sweep, 1), 'LineWidth', 2);
hold on;
plot(s1_sweep, t_q_bar_min_toll, 'LineWidth', 2);
plot(s1_sweep, t_q_bar_processed_min, '--o', 'LineWidth', 2, 'MarkerSize', 10);
axis tight;
axes = gca;
axes.XAxis.FontSize = 15;
axes.XAxis.FontName = 'ubuntu';
axes.YAxis.FontSize = 15;
axes.YAxis.FontName = 'ubuntu';
axes.XLabel.FontName = 'ubuntu';
axes.XLabel.FontWeight = 'bold';
axes.XLabel.String = 'Fraction of fast lane capacity';
axes.XLabel.FontSize = 20;
axes.YLabel.FontName = 'ubuntu';
axes.YLabel.FontWeight = 'bold';
axes.YLabel.String = 'Average queuing delay (processed)';
axes.YLabel.FontSize = 20;
% b_star vs. p_star
subplot(1,3,3);
plot(s1_sweep, p_star_tstar, 'LineWidth', 2);
hold on;
plot(s1_sweep, b_star_tstar, '--o', 'LineWidth', 2, 'MarkerSize', 10);
axis tight;
axes = gca;
axes.XAxis.FontSize = 15;
axes.XAxis.FontName = 'ubuntu';
axes.YAxis.FontSize = 15;
axes.YAxis.FontName = 'ubuntu';
axes.XLabel.FontName = 'ubuntu';
axes.XLabel.FontWeight = 'bold';
axes.XLabel.String = 'Fraction of fast lane capacity';
axes.XLabel.FontSize = 20;
axes.YLabel.FontName = 'ubuntu';
axes.YLabel.FontWeight = 'bold';
axes.YLabel.String = 'Average travel cost';
axes.YLabel.FontSize = 20;
% Average travel cost
subplot(1,3,2);
plot(s1_sweep, c_bar_nom * ones(n_s1_sweep, 1), 'LineWidth', 2);
hold on;
plot(s1_sweep, c_bar_toll, 'LineWidth', 2);
plot(s1_sweep, c_bar, '--o', 'LineWidth', 2, 'MarkerSize', 10);
axis tight;
axes = gca;
axes.XAxis.FontSize = 15;
axes.XAxis.FontName = 'ubuntu';
axes.YAxis.FontSize = 15;
axes.YAxis.FontName = 'ubuntu';
axes.XLabel.FontName = 'ubuntu';
axes.XLabel.FontWeight = 'bold';
axes.XLabel.String = 'Fraction of fast lane capacity';
axes.XLabel.FontSize = 20;
axes.YLabel.FontName = 'ubuntu';
axes.YLabel.FontWeight = 'bold';
axes.YLabel.String = 'Average travel cost';
axes.YLabel.FontSize = 20;